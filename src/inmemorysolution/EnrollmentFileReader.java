import question5.BO.Enrollee;

import java.io.*;

public class EnrollmentFileReader {

    private BufferedReader _enrollmentFileReader = null;
    private EnrollmentFileReaderService _enrollmentFileReaderService = new EnrollmentFileReaderService();

    public void readEnrollmentFile() throws Exception {

        String currentFileLine;
        String[] enrollees;
        String insuranceCompany;
        Enrollee enrollee = null;

        try {
            _enrollmentFileReader = _enrollmentFileReaderService.createEnrollmentFile();

            while ((currentFileLine = _enrollmentFileReader.readLine()) != null) {

                enrollees = _enrollmentFileReaderService.getEnrolleeByLineFromIncomingEnrollmentCSVFile(currentFileLine);
                insuranceCompany = _enrollmentFileReaderService.getInsuranceCompanyFromIncomingEnrollmentCSVFile(enrollees);

                enrollee = _enrollmentFileReaderService.createEnrolleeObject(enrollees, insuranceCompany);

                _enrollmentFileReaderService.writeEnrolleeToMap(enrollee, insuranceCompany);

            }

        } catch (IOException e) {
            e.printStackTrace();

        }
    }


}
