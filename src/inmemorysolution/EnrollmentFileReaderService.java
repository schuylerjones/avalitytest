import question5.BO.Enrollee;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.PatternSyntaxException;

public class EnrollmentFileReaderService {

    private static final String INCOMING_ENROLLMENT_CSV_FILE = "../AvalityTest/resources/InsuranceUsers.csv";
    private static final String CSV_SPLIT_BY = ",";
    private File _enrollmentFile = new File(INCOMING_ENROLLMENT_CSV_FILE);
    private Map<String, List<Enrollee>> _enrolleesMap = new HashMap<>();

    public String[] getEnrolleeByLineFromIncomingEnrollmentCSVFile(String currentFileLine) throws PatternSyntaxException {
        return currentFileLine.split(CSV_SPLIT_BY);
    }

    public String getInsuranceCompanyFromIncomingEnrollmentCSVFile(String[] enrollees) throws PatternSyntaxException {
        return enrollees[4].replaceAll("\\s+", "");
    }

    public BufferedReader createEnrollmentFile() throws FileNotFoundException {
        return new BufferedReader(new FileReader(_enrollmentFile));
    }

    public Enrollee createEnrolleeObject(String[] enrollees, String insuranceCompany) throws Exception {

    return new Enrollee(enrollees[0], enrollees[1], enrollees[2], Integer.parseInt(enrollees[3]), insuranceCompany);
    }

    public boolean enrolleeMapContainsInsuranceCompany(String insuranceCompany) throws NullPointerException, ClassCastException {
        return _enrolleesMap.containsKey(insuranceCompany);
    }

    public void writeEnrolleeToMap(Enrollee enrollee, String insuranceCompany) throws Exception {

        if (enrolleeMapContainsInsuranceCompany(insuranceCompany)) {
            List<Enrollee> enrolleeList = _enrolleesMap.get(insuranceCompany);
            enrolleeList.add(enrollee);
        } else {
            List<Enrollee> newEnrolleeList = new ArrayList<>();
            newEnrolleeList.add(enrollee);
            _enrolleesMap.put(insuranceCompany, newEnrolleeList);
        }
    }
}
