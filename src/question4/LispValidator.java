package question4;

import java.util.Stack;

public class LispValidator {


    private static final Character OPEN_PARENTHESIS = '(';
    private static final Character CLOSED_PARENTHESIS = ')';
    private Stack<Character> stack = new Stack<Character>();

    public boolean isParenthesisClosedNested(String list) {
        try {

            for (int i = 0; i < list.length(); i++) {
                char listCharacter = list.charAt(i);

                if (isOpenParenthesis(listCharacter)) {
                    push(listCharacter);
                }

                if (isClosedParenthesis(listCharacter)) {
                    pop();
                }
            }

            return isStackEmpty();

        } catch (Exception e) {
            return false;
        }
    }

    private boolean isOpenParenthesis(Character listCharacter) {
        return listCharacter.equals(OPEN_PARENTHESIS);
    }

    private boolean isClosedParenthesis(Character listCharacter) {
        return listCharacter.equals(CLOSED_PARENTHESIS);
    }

    private void push(char listCharacter){
        stack.push(listCharacter);
    }

    private void pop(){
        stack.pop();
    }

    private boolean isStackEmpty() {
        return stack.isEmpty();
    }

    public static void main(String[]args){
        LispValidator v = new LispValidator();
        String list = "( i am a list()";

       System.out.println(v.isParenthesisClosedNested(list));

    }

}
