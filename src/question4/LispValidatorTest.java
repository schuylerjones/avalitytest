package question4;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class LispValidatorTest {

    LispValidator lispValidator = new LispValidator();

    @Test
    public void testisParenthesisClosedNested() {

        String test = " ( i am a list)";
        boolean value = lispValidator.isParenthesisClosedNested(test);

        Assert.assertEquals(true, value);

    }

    @Test
    public void testisParenthesisClosedNestedfalse() {

        String test = "( ( a b c)";
        boolean value = lispValidator.isParenthesisClosedNested(test);

        Assert.assertEquals(false, value);

    }
}