package question5.test;

import org.junit.Assert;
import org.junit.Test;
import question5.BO.Enrollee;
import question5.service.stream.StreamSplitter;
import question5.service.stream.StreamSplitterSortService;

import java.io.File;
import java.util.Comparator;
import java.util.List;

public class StreamEnrollmentFileReaderServiceTest {

    private StreamSplitter streamSplitter = new StreamSplitter();
    private StreamSplitterSortService streamSplitterSortService = new StreamSplitterSortService();
    private String[] e;

    @Test
    public void testprocessEnrollmentFile() throws Exception {

        List<File> tempFileList = streamSplitter.splitFileIntoChunks(new File("../AvalityTest/resources/InsuranceUsers.csv"));

        Comparator<Enrollee> comp = new Comparator<Enrollee>() {
            public int compare(Enrollee o1, Enrollee o2) {
                return o1.getInsuranceCompany().compareTo(o2.getInsuranceCompany());
            }
        };
        int result = streamSplitterSortService.mergeTempFiles(tempFileList, new File("../AvalityTest/resources/outputfile.csv"), comp);
        Assert.assertTrue(result > 0);
    }

}
