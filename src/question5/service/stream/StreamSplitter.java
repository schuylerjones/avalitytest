package question5.service.stream;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * this class is responsible for reading the file and split the file in chunks
 */
public class StreamSplitter {

    private StreamSplitterService _streamSplitterService = new StreamSplitterService();
    private StreamSplitterSortService _streamSplitterSortService = new StreamSplitterSortService();

    // this function will load the file by chuncks of x rows
    //then write the results to temp files that
    //will be merged later
    //return a list of temp files
    public List<File> splitFileIntoChunks(File fileToBeSorted) throws IOException {

        List<File> temporaryFilesList = new ArrayList<File>();
        BufferedReader br = new BufferedReader(new FileReader(fileToBeSorted));
        long maxChunkSize = _streamSplitterService.estimateMaxSizeOfChunks(fileToBeSorted);
        List<String> fileChunksList = null;

        try {
            fileChunksList = new ArrayList<String>();
            String currentLine = "";
            while (currentLine != null) {
                long currentChunkSize = 0; //in bytes
                while ((currentChunkSize < maxChunkSize) && ((currentLine = br.readLine()) != null)) {

                    fileChunksList.add(currentLine);
                    currentChunkSize += currentLine.length();
                }
                temporaryFilesList.add(_streamSplitterService.writeEnrolleeToTempFile(fileChunksList));
                fileChunksList.clear();
            }
        } catch (EOFException e) {
                if(fileChunksList.size() > 0) {
                    temporaryFilesList.add(_streamSplitterService.writeEnrolleeToTempFile(fileChunksList));
                }
        } finally {
            br.close();
        }

        return temporaryFilesList;
    }

}
