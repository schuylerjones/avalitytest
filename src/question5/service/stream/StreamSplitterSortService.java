package question5.service.stream;


import question5.BO.Enrollee;
import question5.service.buffer.BinaryFileBuffer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

public class StreamSplitterSortService {

    private StreamSplitterService splitterService = new StreamSplitterService();

    public int mergeTempFiles(List<File> tempFileList, File outputfile, final Comparator<Enrollee> cmp) throws IOException {

        PriorityQueue<BinaryFileBuffer> bufferQueue = new PriorityQueue<BinaryFileBuffer>();

        addBufferObject(bufferQueue, tempFileList);

        BufferedWriter fbwOutputFile = new BufferedWriter(new FileWriter(outputfile));
        int rowcounter = 0;
        try {
            while (bufferQueue.size() > 0) {
                BinaryFileBuffer bfb = bufferQueue.poll();
                String cacheData = bfb.pop(); //data in temp file
                fbwOutputFile.write(cacheData);
                fbwOutputFile.newLine();
                ++rowcounter;

                if (bfb.empty()) {
                    bfb.fbr.close();
                    bfb.originalfile.delete();// we don't need you anymore

                } else {
                    bufferQueue.add(bfb); // add it back
                }
            }

        } finally {
            fbwOutputFile.close();
            for (BinaryFileBuffer bfb : bufferQueue) bfb.close();
        }

        return rowcounter;

    }

    public void addBufferObject(PriorityQueue<BinaryFileBuffer> queue, List<File> tempFileList) throws IOException {
        //add buffer object to priority queue
        for (File tempFile : tempFileList) {
            BinaryFileBuffer buffer = createBinaryFileBufferObject(tempFile);
            queue.add(buffer);
        }

    }

    public BinaryFileBuffer createBinaryFileBufferObject(File tempFile) throws IOException {
        return new BinaryFileBuffer(tempFile);
    }


}
