package question5.service.stream;

import question5.BO.Enrollee;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class StreamSplitterService {

    private static final String CSV_SPLIT_BY = ",";

    //we divide the file into small chunks. if the chunks
    //are too small we create more temp files
    //if too big we use too much memory
    public long estimateMaxSizeOfChunks(File fileToBeSorted) {
        long sizeOfFile = fileToBeSorted.length();
        // we dont want to open up more than 1024 temp files
        //for the sake of practice problem we will set to 6
        final int MAXTEMPFILES = 3;
        long maxChunkSize = sizeOfFile / MAXTEMPFILES;
        //we dont want to create too many temp files
        //if chunk size is smaller than half the free memory grow it
        //long freeMemory = Runtime.getRuntime().freeMemory();
        //for the sake of practice set to 40 (bytes)
        long freeMemory = 40;
        if (maxChunkSize < freeMemory)
            maxChunkSize = freeMemory / 2;
        else {
            if (maxChunkSize >= freeMemory)
                System.err.println("Expect to run out of memory");
        }
        return maxChunkSize;
    }

    public File writeEnrolleeToTempFile(List<String> enrolleeFileList) throws IOException {

        File newTempFile = File.createTempFile("sortInChunks", "tempfile");
        newTempFile.deleteOnExit();
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(newTempFile));
        try {

            for (String enrollee : enrolleeFileList) {

                bufferedWriter.write(enrollee);

                bufferedWriter.newLine();
            }
        } finally {
            bufferedWriter.close();
        }
        return newTempFile;
    }

    public String[] getEnrolleesByLine(String currentLine) {
        return currentLine.split(CSV_SPLIT_BY);
    }

    public Enrollee createEnrolleeObject(String[] enrollees) {
        return new Enrollee(enrollees[0], enrollees[1], enrollees[2], Integer.parseInt(enrollees[3]), enrollees[4]);
    }


}
