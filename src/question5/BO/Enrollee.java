package question5.BO;

public class Enrollee {

    private String userId;
    private String firstName;
    private String lastName;
    private Integer version;
    private String insuranceCompany;

    public Enrollee(String userId, String firstName, String lastName, Integer version, String insuranceCompany) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.version = version;
        this.insuranceCompany = insuranceCompany;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(String insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    @Override
    public String toString() {
        return "Enrollees [userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", version="
                + version + ", insuranceCompany=" + insuranceCompany + "]";
    }
}
